// #include "solution.h"
#include <fstream>
// импорты для задачи

struct Input {
  /* зависит от задачи */
};

struct Output {
  /* зависит от задачи */
};

Input ParseInput(std::ifstream& input) {
  /* зависит от задачи */
}

Output CallUserSolution(const Input& input) {
  /* зависит от задачи */
}

bool Validate(const Input& input, const Output& output) {
  /* зависит от задачи */
}

bool Test(const std::string& path) {
  std::ifstream input_file(path);
  const auto input = ParseInput(input_file);
  const auto output = CallUserSolution(input);
  return Validate(input, output);
}