# import Solution
# импорты для задачи

class Input:
    # зависит от задачи


class Output:
    # зависит от задачи


def ParseInput(file_input_) -> Input:
    # зависит от задачи


def CallUserSolution(input_: Input) -> Output:
    # зависит от задачи


def Validate(input_: Input, output_: Output) -> bool:
    # зависит от задачи


def Test(path_: str) -> bool:
    file = open(path_, "r")
    input_test = ParseInput(file)
    file.close()
    output_test = CallUserSolution(input_test)
    return Validate(input_test, output_test)