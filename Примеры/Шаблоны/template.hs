-- import Solution 
import Data.Bool (Bool)
import GHC.IO (FilePath, IO)
import GHC.IO.IOMode (IOMode(ReadMode))
import GHC.IO.Handle
import System.IO (openFile)
-- импорты для задачи

data Input = -- зависит от задачи

data Output = -- зависит от задачи

parseInput :: Handle -> IO Input
parseInput file = -- зависит от задачи

callUserSolution :: Input -> Output
callUserSolution = -- зависит от задачи

validate :: Input -> Output -> Bool
validate = -- зависит от задачи

test :: FilePath -> IO Bool 
test path = do
    file <- openFile path ReadMode
    input <- parseInput file 
    hClose file
    let output = callUserSolution input
    return $ validate input output