from typing import List


class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        pass


class Input:
    def __init__(self, nums_, target_):
        self.nums = nums_
        self.target = target_


class Output:
    def __init__(self, l_):
        self.indexes = l_


def ParseInput(file_input_) -> Input:
    line = file_input_.read().strip().split()
    nums = list(map(int, line[1:-1]))
    target = int(line[-1])
    return Input(nums, target)


def CallUserSolution(input_: Input) -> Output:
    solution = Solution()
    return Output(solution.twoSum(input_.nums, input_.target))


def Validate(input_: Input, output_: Output) -> bool:
    if len(output_.indexes) != 2 or output_.indexes[0] == output_.indexes[1]:
        return False
    return output_.indexes[0] + output_.indexes[1] == input_.target


def Test(path_: str) -> bool:
    file = open(path_, "r")
    input_test = ParseInput(file)
    file.close()
    output_test = CallUserSolution(input_test)
    return Validate(input_test, output_test)