#include <fstream>
#include <vector>

class Solution {
public:
  std::vector<int> twoSum(const std::vector<int>& nums, int target) {
      return {};
  }
};

struct Input {
  std::vector<int> nums;
  int target;
};

struct Output {
  std::vector<int> indexes;
};

Input ParseInput(std::ifstream& input) {
  int nums_size;
  input >> nums_size;
  std::vector<int> nums(nums_size);
  for (int i = 0; i < nums_size; ++i) {
    input >> nums[i];
  }
  int target;
  input >> target;
  return {nums, target};
}

Output CallUserSolution(const Input& input) {
  Solution solution;
  std::vector<int> answer = solution.twoSum(input.nums, input.target);
  return {answer};
}

bool Validate(const Input& input, const Output& output) {
  if (output.indexes.size() != 2 || output.indexes[0] == output.indexes[1]) {
    return false;
  }
  return output.indexes[0] + output.indexes[1] == input.target;
}

bool Test(const std::string& path) {
  std::ifstream input_file(path);
  const auto input = ParseInput(input_file);
  const auto output = CallUserSolution(input);
  return Validate(input, output);
}