import Data.Bool (Bool)
import GHC.IO (FilePath, IO)
import GHC.IO.IOMode (IOMode(ReadMode))
import GHC.IO.Handle
import System.IO (openFile)
-- ещё могут быть импорты

solve :: [Int] -> Int -> [Int]
solve nums target = []

data Input = Input { getNums :: [Int], getTarget :: Int }

data Output = Output { getAnswer :: [Int] }

parseInput :: Handle -> IO Input
parseInput file = do
    content <- hGetContents file
    let (target : values) = words content
    return $ Input (map read values) (read target)

callUserSolution :: Input -> Output
callUserSolution (Input nums target) = Output $ solve nums target

validate :: Input -> Output -> Bool
validate (Input nums target) (Output answer) = (length answer == 2) && (i1 /= i2) && (v1 + v2 == target)
    where
        (v1, v2) = (nums !! i1, nums !! i2)
        [i1, i2] = answer

test :: FilePath -> IO Bool 
test path = do
    file <- openFile path ReadMode
    input <- parseInput file 
    hClose file
    let output = callUserSolution input
    return $ validate input output