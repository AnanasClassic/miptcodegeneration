import gettext

def main():
    ja = gettext.translation('translator', localedir='locale', languages=['ja'])
    ja.install()

    name = _("Maxim")
    print(_("Hello, %s!") % name)
    print(_("What's up?"))
    print(_("Buy %s") % _("a bun"))
    print(_("How many?"))
    count = _("34")
    print(_("%s buns") % count)
    print(_("Guitar!"))

if __name__ == "__main__":
    main()