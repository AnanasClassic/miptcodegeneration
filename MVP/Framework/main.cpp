#include <iostream>
#include "Language/Language.hpp"
#include "Language/Type/BaseType.hpp"
#include "Language/Type/TemplateType.hpp"

int main() {
    auto input_fields = std::vector<CompositeTypeField>();
    input_fields.emplace_back(L"target", std::make_unique<BaseType>(
            BaseTypeKind::Int
    ));
    input_fields.emplace_back(L"nums", std::make_unique<TemplateType>(
            TemplateTypeKind::Array,
            std::make_unique<BaseType>(BaseTypeKind::Int)
    ));

    auto output_fields = std::vector<CompositeTypeField>();
    output_fields.emplace_back(L"output", std::make_unique<BaseType>(
            BaseTypeKind::Int
    ));

    const auto input = InputType(L"Input", std::move(input_fields));
    const auto output = OutputType(L"Output", std::move(output_fields));

    const auto userSolutionInfo = UserSolutionInfo{L"twoSum", input, output};

    const auto language = std::make_unique</* ЯП */>();
    const auto result = language->GetAdditionalCode()
            + language->GetTypeDeclaration(input)
            + language->GetTypeDeclaration(output)
            + language->GetParseInputFunction(input)
            + language->GetCallUserSolution(userSolutionInfo)
            + language->GetTestingCode(userSolutionInfo);

    std::wcout << result;

    std::wcout << L"\n\n\n";
    std::wcout << language->GetUserSolutionTemplateCode(userSolutionInfo);
}