#pragma once

#include <string>

#include "Type/Type.hpp"
#include "UserSolutionInfo.hpp"

struct Language {
    [[nodiscard]] virtual std::wstring GetAdditionalCode() const = 0;

    [[nodiscard]] virtual std::wstring GetTypeDeclaration(const CompositeType &type) const = 0;

    [[nodiscard]] virtual std::wstring GetParseInputFunction(const InputType &inputType) const = 0;

    [[nodiscard]] virtual std::wstring GetCallUserSolution(const UserSolutionInfo &info) const = 0;

    [[nodiscard]] virtual std::wstring GetTestingCode(const UserSolutionInfo &info) const = 0;

    [[nodiscard]] virtual std::wstring GetUserSolutionTemplateCode(const UserSolutionInfo &info) const = 0;
};
