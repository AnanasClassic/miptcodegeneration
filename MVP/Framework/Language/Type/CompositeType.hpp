#pragma once

#include <string>
#include <utility>
#include <vector>
#include <memory>
#include "Type.hpp"

struct CompositeTypeField {
    CompositeTypeField(std::wstring name, std::unique_ptr<Type> type) : name(std::move(name)), type(std::move(type)) {}

    std::wstring name;
    std::unique_ptr<Type> type;
};

struct CompositeType : Type {
    CompositeType(std::wstring name, std::vector<CompositeTypeField> fields) : name(std::move(name)),
                                                                               fields(std::move(fields)) {}

    std::wstring name;
    std::vector<CompositeTypeField> fields;
};