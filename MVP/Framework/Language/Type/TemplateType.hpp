#pragma once

#include <memory>
#include "Type.hpp"

enum class TemplateTypeKind {
    Array
};

struct TemplateType : Type {
    TemplateType(TemplateTypeKind kind, std::unique_ptr<Type> underlying) : kind(kind),
                                                                            underlying(std::move(underlying)) {}

    TemplateTypeKind kind;
    std::unique_ptr<Type> underlying;
};
