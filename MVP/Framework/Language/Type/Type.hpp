#pragma once

struct Type {
    template <class T>
    const T *As() const {
        return dynamic_cast<const T*>(this);
    }

    template <class T>
    T *As() {
        return dynamic_cast<T*>(this);
    }

    virtual ~Type() = default;
};
