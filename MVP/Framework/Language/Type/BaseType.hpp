#pragma once

#include "Type.hpp"

enum class BaseTypeKind {
    Int,
    Real,
    String
};

struct BaseType : Type {
    explicit BaseType(BaseTypeKind kind) : kind(kind) {}

    BaseTypeKind kind;
};