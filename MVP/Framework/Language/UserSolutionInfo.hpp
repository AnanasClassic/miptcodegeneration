#pragma once

#include <string>
#include "Type/Type.hpp"
#include "Type/CompositeType.hpp"

using InputType = CompositeType;
using OutputType = CompositeType;

struct UserSolutionInfo {
    std::wstring methodName;
    const InputType &inputType;
    const OutputType &outputType;
};